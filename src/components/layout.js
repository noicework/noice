import React from "react"
import { Link } from "gatsby"
import Animation from '../components/animations';

const Layout = ({ location, title, description, children }) => {
  const rootPath = `${__PATH_PREFIX__}/`
  const isRootPath = location.pathname === rootPath
  let header

  if (isRootPath) {
    header = (
      <div className="p-6 max-w-sm mx-auto bg-white rounded-xl shadow-md flex items-center space-x-4">
      <div className="flex-shrink-0">
        <img className="h-12 w-12" src="/img/logo.svg" alt="ChitChat Logo" />
      </div>
      <div>
        <div className="text-xl font-medium text-black">TEST!@!!</div>
        <p className="text-gray-500">You have a new message!</p>
      </div>
      </div>
    )
  } else {
    header = (
      <div>
      <Link className="header-link-home" to="/">
        {title}
      </Link>
      <span>{description}</span>
      </div>
    )
  }

  return (
    <div className="global-wrapper" data-is-root-path={isRootPath}>
      <a className="skip-to-content-link" href="#main-content">
        Skip to content
      </a>
      <header className="global-header">{header}</header>
      <main id="main-content">{children}</main>
      <footer className="main-footer">
        <nav className="links" aria-label="Personal links">
          <ul>
            <li><a href="/about">About</a></li>
            <li><a href="https://twitter.com/jaketracey">Twitter</a></li>
            <li><a href="https://linkedin.com/in/jaketracey">LinkedIn</a></li>
          </ul>
        </nav>
        <p>All great deeds and all great thoughts have a ridiculous beginning. Great works are often born on a street corner or in a restaurant's revolving door. - Albert Camus</p>
      </footer>
    </div>
  )
}

export default Layout
